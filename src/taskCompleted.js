import * as React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';
import { completedTaskService, categoryService } from './services';
import { Alert, Card, Row, Column, Icon } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders Task Completed component
 *
 * Methods:
 *
 * delete() -> Deletes task and rerenders TaskCompleted component
 *
 * sortAlfa() -> Sorts completed task names alfabetically
 *
 * dateSort() -> Sorts completed tasks by deadline
 *
 * prioSort() Sorts completed tasks by priority (first click, all tasks with priority first)
 */

class TaskCompleted extends Component {
  tasks = [];
  categorys = [];
  onlyPressedOnce = false;
  onlyPressedOncePrio = false;
  onlyPressedOnceDeadline = false;

  render() {
    return (
      <div>
        <Card>
          <Row>
            <Column>
              Task-name <Icon.Alfa onClick={this.sortAlfa} />
            </Column>
            <Column>
              Priority <Icon.Prio onClick={this.prioSort} />
            </Column>
            <Column>
              Category <Icon.Prio style={{ pointerEvents: 'none' }} onClick={this.sortCategory} />
            </Column>
            <Column>Completed</Column>
            <Column> </Column>
          </Row>
          <br />
          {this.tasks.map((task) => (
            <div key={task.id} className="taskdiv">
              <Row key={task.id}>
                <Column title="Task-name">
                  <NavLink className="catobject" to={'/completed/' + task.id}>
                    {task.name}
                  </NavLink>
                </Column>
                <Column style={{ marginTop: '0.2rem' }}>
                  {task.priority ? (
                    <Icon.Priority style={{ pointerEvents: 'none' }} />
                  ) : (
                    <Icon.NoPriority style={{ pointerEvents: 'none' }} />
                  )}
                </Column>
                <Column style={{ marginTop: '0.31rem' }} title="Category">
                  {this.categorys.map((cate) => (task.categoryId == cate.id ? cate.name : ' '))}
                </Column>
                <Column style={{ marginTop: '0.31rem' }}>{task.Completedate}</Column>
                <Column style={{ marginTop: '0.3rem' }}>
                  <div
                    alt="Task delete button"
                    title="Press to delete"
                    style={{ display: 'inline' }}
                  >
                    <Icon.Trash
                      onClick={() => {
                        this.delete(task);
                      }}
                    />
                  </div>
                </Column>
              </Row>
            </div>
          ))}
        </Card>
      </div>
    );
  }

  mounted() {
    completedTaskService
      .getTasks()
      .then((tasks) => {
        this.tasks = tasks;
        categoryService.getCategorys().then((category) => {
          this.categorys = category;
        });
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  delete(task) {
    completedTaskService
      .deleteTask(task)
      .then(TaskCompleted.instance().mounted)
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  sortAlfa() {
    if (this.onlyPressedOnce) {
      this.tasks.sort(function (a, b) {
        if (a.name > b.name) {
          return -1;
        }
        if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnce = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnce = true;
    }
  }

  sortCategory() {
    if (this.onlyPressedOnceCategory) {
      this.tasks.sort(function (a, b) {
        if (a.categoryId > b.categoryId) {
          return -1;
        }
        if (a.categoryId < b.categoryId) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceCategory = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.categoryId < b.categoryId) {
          return -1;
        }
        if (a.categoryId > b.categoryId) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceCategory = true;
    }
  }
  dateSort() {
    if (this.onlyPressedOnceDeadline) {
      this.tasks.sort(function (a, b) {
        if (a.endDate > b.endDate) {
          return -1;
        }
        if (a.endDate < b.endDate) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceDeadline = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.endDate < b.endDate) {
          return -1;
        }
        if (a.endDate > b.endDate) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnceDeadline = true;
    }
  }

  prioSort() {
    if (this.onlyPressedOncePrio) {
      this.tasks.sort(function (a, b) {
        if (a.priority < b.priority) {
          return -1;
        }
        if (a.priority > b.priority) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOncePrio = false;
    } else {
      this.tasks.sort(function (a, b) {
        if (a.priority > b.priority) {
          return -1;
        }
        if (a.priority < b.priority) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOncePrio = true;
    }
  }
}

export default TaskCompleted; // Exports this Componant to be used in index.js
