import * as React from 'react';
import { Component } from 'react-simplified';
import { categoryService } from './services';
import { Alert, Card, Row, Column, Button, Form } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders Category New component
 *
 * Methods:
 *
 * add() -> Adds the new category to the database, and redirects the user to CategoryList
 *
 * cancel() ->  Redirects the user to CategoryList
 */

class CategoryNew extends Component {
  categorys = [];

  category = {
    name: '',
    description: '',
  };

  render() {
    if (!this.category) return null;

    return (
      <div>
        <Card title="New Category">
          <Row>
            <Column>
              {' '}
              <Form.Label>Name:</Form.Label>
              <Form.Input
                type="text"
                id={"NewCategory"}
                value={this.category.name}
                onChange={(event) => (this.category.name = event.currentTarget.value)}       // addes a value for category name  
              />
              <Form.Label>Description</Form.Label>
              <Form.Input
                type="text"
                value={this.category.description}
                onChange={(event) => (this.category.description = event.currentTarget.value)}      // addes a value for category discription  
              />
            </Column>
          </Row>
        </Card>
        <Card>
          <Row>
            <Column>
              <Button.Success
                onClick={() => {
                  if (this.category.name == '') {
                    Alert.info('You must specify category name');
                    document.getElementById('NewCategory').style.borderColor = 'red'
                  } else {
                    this.add();
                  }
                }}
              >
                Save
              </Button.Success>
            </Column>
            <Column right>
              <Button.Light onClick={this.cancel}>Cancel</Button.Light>
            </Column>
          </Row>
        </Card>
      </div>
    );
  }
  mounted() {
    categoryService
      .getCategorys()
      .then((categorys) => {
        this.categorys = categorys;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  add() {
    categoryService.newCategory(this.category).then(() => {
      history.push('/categorys/');
    });
  }
  cancel() {
    history.push('/categorys');
  }
}

export default CategoryNew;         // Exports this Componant to be used in index.js