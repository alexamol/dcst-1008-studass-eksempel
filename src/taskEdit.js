import * as React from 'react';
import { Component } from 'react-simplified';
import { taskService, categoryService } from './services';
import { Alert, Card, Row, Column, Button, Form } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders TaskEdit component
 *
 * Methods:
 *
 * save() -> Updates the task in the database with the changes made, redirects user to the TaskDetails component for the spesiffic task.
 *
 * cancel() -> Redirects user to the TaskDetails component for the spesiffic task.
 */

class TaskEdit extends Component {
  task = null;
  categorys = [];
  valgtecategory = null;

  render() {
    if (!this.task) return null;
    if (!this.valgtecategory) return null;
    return (
      <div>
        <Card title="Edit task">
          <Form.Textarea
            type="text"
            id={"NewTask"}
            value={this.task.name}
            onChange={(event) => (this.task.name = event.currentTarget.value)}          // Edites the value of task name
          >
            <label>Task-name</label>
          </Form.Textarea>
          <Row>
            <Column>
              <Form.Label>Start date:</Form.Label>
              <Form.Input
                type="date"
                id={"StartDate"}
                pattern={"[0-9]{4}-[0-9]{2}-[0-9]{2}"}
                value={this.task.startDate}
                onChange={(event) => (this.task.startDate = event.currentTarget.value)}       // Edites the value of task Start date
              />
            </Column>
            <Column>
              <Form.Label>End date:</Form.Label>
              <Form.Input
                type="date"
                id={"EndDate"}
                pattern={"[0-9]{4}-[0-9]{2}-[0-9]{2}"}
                value={this.task.endDate}
                onChange={(event) => (this.task.endDate = event.currentTarget.value)}          // Edites the value of task End date
              />
            </Column>
          </Row>

          <Row>
            <Column>
              <Form.Label>Category:</Form.Label>

              <select
                className="form-select"
                aria-label="Default select example"
                defaultValue={this.valgtecategory.id}
                value={this.categorys.name}
                onChange={(event) => (this.task.categoryId = event.currentTarget.value)}       //  Changes the value of the chosen category 
              >
                {this.categorys.map((category) => (                                    // lists all the categories in the select tag
                  <option key={category.id} value={category.id}>
                    {category.name}
                  </option>
                ))}
              </select>
            </Column>
            <Column>
              <Form.Label>Priority</Form.Label>
              <Form.Switch
                checked={this.task.priority}
                onChange={() => {                                                   // Switch to toggle Priority on or off
                  document.getElementById('flexSwitchCheckDefault').checked
                    ? (this.task.priority = 1)
                    : (this.task.priority = 0);
                }}
              ></Form.Switch>
            </Column>
          </Row>
          <Row>
            <Form.Label></Form.Label>
            <Form.Textarea
              defaultValue={this.task.description}
              onChange={(event) => (this.task.description = event.currentTarget.value)}           // Changes the value of a task discription 
            >
              <label>Description</label>
            </Form.Textarea>
          </Row>
        </Card>
        <Row>
          <Column>
            <Button.Success
              onClick={() => {
                if (this.task.name == '') {
                  Alert.info('You must specify task name');
                  document.getElementById('NewTask').style.borderColor = 'red';
                  if (this.task.startDate == '') {
                    document.getElementById('StartDate').style.borderColor = 'red';
                  } else if (this.task.endDate == '') {
                    document.getElementById('EndDate').style.borderColor = 'red';
                  }
                  if (this.task.startDate == '' && this.task.endDate == '') {
                    document.getElementById('StartDate').style.borderColor = 'red';
                    document.getElementById('EndDate').style.borderColor = 'red';
                  }
                } else if (this.task.startDate == '' || this.task.endDate == '') {
                  Alert.info('You must specify task date');
                  this.task.startDate == ''
                    ? (document.getElementById('StartDate').style.borderColor = 'red')
                    : '';
                  this.task.endDate == ''
                    ? (document.getElementById('EndDate').style.borderColor = 'red')
                    : '';
                  if (this.task.startDate == '' && this.task.endDate == '') {
                    document.getElementById('StartDate').style.borderColor = 'red';
                    document.getElementById('EndDate').style.borderColor = 'red';
                  }
                } else {
                  this.save();
                }
              }}
            >
              Save
            </Button.Success>
          </Column>
          <Column right>
            <Button.Light onClick={this.cancel}>Cancel</Button.Light>
          </Column>
        </Row>
      </div>
    );
  }

  mounted() {
    taskService
      .getTask(this.props.match.params.id)
      .then((task) => {
        this.task = task;
        taskService
          .getCategory(task.categoryId)
          .then((cate) => {
            this.valgtecategory = cate;
          })
          .catch((error) => {
            Alert.danger(error.message);
          });
      })
      .catch((error) => {
        Alert.danger(error.message);
      });

    categoryService
      .getCategorys()
      .then((categorys) => {
        this.categorys = categorys;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  save() {
    if(this.task.endDate < this.task.startDate){
      Alert.danger("End date cannot be before start date")}
      else {
    taskService
      .updateTask(this.task)
      .then(() => {
        history.push('/tasks/' + this.props.match.params.id);
      })
      .catch((error) => {
        Alert.danger(error.message);
      });}
  }

  cancel() {
    history.push('/tasks/' + this.props.match.params.id);
  }
}

export default TaskEdit;      // Exports this Componant to be used in index.js
