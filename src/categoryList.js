import * as React from 'react';
import { Component } from 'react-simplified';
import { NavLink } from 'react-router-dom';
import { categoryService, taskService } from './services';
import { Alert, Card, Row, Column, Button, Icon } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders Category List component
 *
 * Methods:
 *
 * sortAlfa() -> Sorts category names alfabetically
 */

class CategoryList extends Component {
  categorys = [];
  tasks = [];
  onlyPressedOnce = false;

  render() {
    return (
      <div>
        <Card>
          <Row>
            <Column>
             Categories <Icon.Alfa onClick={this.sortAlfa} />
            </Column>
            <Column>Tasks</Column>
          </Row>
          <br />
          {this.categorys.map((category) => (                // Map function to list all the categories
            <Row key={category.id}>
              <Column>
                <NavLink className="catobject" to={'/categorys/' + category.id}>  
                  {category.name}
                </NavLink>
              </Column>
              <Column>{this.tasks.filter((task) => task.categoryId == category.id).length}</Column>        {/*Filters how many tasks are there in a specific category, and count them with .length*/}
            </Row>  
          ))}
        </Card>

        <NavLink to={'/categorys/:id/new'}>
          <button className="new-task">New category</button>
        </NavLink>
      </div>
    );
  }

  mounted() {
    categoryService
      .getCategorys()
      .then((categorys) => {
        this.categorys = categorys;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });

    taskService
      .getTasks()
      .then((tasks) => {
        this.tasks = tasks;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }
  sortAlfa() {
    if (this.onlyPressedOnce) {                       // Sorts the list from alphabetically from A to C
      this.categorys.sort(function (a, b) {
        if (a.name > b.name) {
          return -1;
        }
        if (a.name < b.name) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnce = false;
    } else {
      this.categorys.sort(function (a, b) {          // Sorts the list from alphabetically from  C to A  if the sort button is pressed again
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });
      this.onlyPressedOnce = true;
    }
  }
}

export default CategoryList;          // Exports this Componant to be used in index.js
