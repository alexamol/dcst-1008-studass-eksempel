import { pool } from './mysql-pool';

/**
 * @TaskService class
 *
 * getTasks() -> Gets all tasks from the tasks database
 *
 * getTask() -> Gets 1 task from the tasks database spessified by id
 *
 * getCategory() -> Gets 1 category from the categorys database spessified by id
 *
 * updateTask() -> Updates properties of 1 task in the Tasks database spessified by id
 *
 * deleteTask() -> Deletes 1 task from the Tasks database spessified by id
 *
 * newTask() -> Generates a new task with all properies availible in the Tasks database
 */

class TaskService {
  getTasks() {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Tasks', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getTask(id) {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Tasks WHERE id=?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  getCategory(id) {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Categorys WHERE id=?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  updateTask(task) {
    return new Promise((resolve, reject) => {
      pool.query(
        'UPDATE Tasks SET name=?, description=?, categoryId=?, startDate=?, endDate=?, priority=? WHERE id=?',
        [
          task.name,
          task.description,
          task.categoryId,
          task.startDate,
          task.endDate,
          task.priority,
          task.id,
        ],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
  updatePrio(task) {
    return new Promise((resolve, reject) => {
      pool.query(
        'UPDATE Tasks SET priority=? WHERE id=?',
        [task.priority, task.id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
  deleteTask(task) {
    return new Promise((resolve, reject) => {
      pool.query('DELETE from Tasks where id=?', [task.id], (error, results) => {
        if (error) return reject(error);

        resolve();
      });
    });
  }
  newTask(task) {
    return new Promise((resolve, reject) => {
      pool.query(
        'INSERT INTO Tasks (name, description, priority, startDate, endDate, categoryId) VALUES (?, ?, ?, ?, ?, ?)',
        [task.name, task.description, task.priority, task.startDate, task.endDate, task.categoryId],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
}

/**
 * @CategoryService class
 *
 * getCategorys() -> Gets all categorys from the Categorys database
 *
 * getCategory() -> Gets 1 category from the Categorys database spessified by id
 *
 * getDefault() -> Gets all categorys and returns the first one
 *
 * getTasks() -> Gets all tasks with the spessified categoryId
 *
 * newCategory() -> Inserts a new category into the Categorys database with the spessified values
 *
 * deleteCategory -> Deletes 1 category from the Categorys database, spessified by id
 *
 * deleteCategoryAndTasks() -> Deletes all tasks with the spessified categoryId and the category with the same id
 *
 * editCategory() -> Updates properties of 1 category in the Categorys database spessified by id
 */

class CategoryService {
  getCategorys() {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Categorys', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getCategory(id) {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Categorys WHERE id=?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  getDefault() {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Categorys', (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  getTasks(categoryId) {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Tasks WHERE categoryId=?', [categoryId], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  newCategory(category) {
    return new Promise((resolve, reject) => {
      pool.query(
        'INSERT INTO Categorys (name, description) VALUES (?, ?)',
        [category.name, category.description],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
  deleteCategory(category) {
    return new Promise((resolve, reject) => {
      pool.query('DELETE FROM Categorys WHERE id=?', [category.id], (error, results) => {
        if (error) return reject(error);

        resolve();
      });
    });
  }

  deleteCategoryAndTasks(category) {
    return new Promise((resolve, reject) => {
      pool.query(
        'DELETE Tasks, Categorys FROM Tasks INNER JOIN Categorys ON Tasks.categoryId = Categorys.id WHERE Categorys.id=?',
        [category.id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  editCategory(category) {
    return new Promise((resolve, reject) => {
      pool.query(
        'UPDATE Categorys SET name=?, description=? WHERE id=?',
        [category.name, category.description, category.id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
}

/**
 * @NewTaskService class
 *
 * completeTask() -> Creates a new task and inserts it into the CompletedTasks database
 * properties of the task are spessified by the properties of the task supplied to the function
 * with exeption of the completedate wich is supplied by the adddate() function.
 *
 */

class NewTaskService {
  completeTask(task) {
    return new Promise((resolve, reject) => {
      pool.query(
        'INSERT INTO CompletedTasks (name, description, priority, startDate, endDate, categoryId, Completedate) VALUES (?, ?, ?, ?, ?, ?, ?)',
        [
          task.name,
          task.description,
          task.priority,
          task.startDate,
          task.endDate,
          task.categoryId,
          task.Completedate,
        ],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
}

/**
 * @CompletedTaskService
 *
 * getTasks() -> Gets all tasks from the CompletedTasks database
 *
 * getTask() -> Gets 1 task from the CompletedTasks database spessified by id
 *
 * getCategory() -> Gets 1 category from the categorys database spessified by id
 *
 * deleteTask() -> Deletes 1 task from the CompletedTasks database spessified by id
 */

class CompletedTaskService {
  getTasks() {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM CompletedTasks', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }

  getTask(id) {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM CompletedTasks WHERE id=?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  getCategory(id) {
    return new Promise((resolve, reject) => {
      pool.query('SELECT * FROM Categorys WHERE id=?', [id], (error, results) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  deleteTask(task) {
    return new Promise((resolve, reject) => {
      pool.query('DELETE from CompletedTasks where id=?', [task.id], (error, results) => {
        if (error) return reject(error);

        resolve();
      });
    });
  }

  deleteCompletedTasks(category) {
    return new Promise((resolve, reject) => {
      pool.query(
        'DELETE CompletedTasks FROM CompletedTasks INNER JOIN Categorys ON CompletedTasks.categoryId = Categorys.id WHERE Categorys.id=?',
        [category.id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  deleteTasks(category) {
    return new Promise((resolve, reject) => {
      pool.query(
        'DELETE Tasks FROM Tasks INNER JOIN Categorys ON Tasks.categoryId = Categorys.id WHERE Categorys.id=?',
        [category.id],
        (error, results) => {
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
}

/**
 * Exports all task services
 */

export let newTaskService = new NewTaskService();
export let taskService = new TaskService();
export let categoryService = new CategoryService();
export let completedTaskService = new CompletedTaskService();
