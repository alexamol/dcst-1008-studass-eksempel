import * as React from 'react';
import { Component } from 'react-simplified';
import { taskService, categoryService } from './services';
import { Card, Row, Column, Button, Form, Alert } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders TaskNew
 *
 * Methods:
 *
 * save() -> Generates new task and redirects the user to the TaskList component
 *
 * cancel() -> Redirects the user to the TaskList component
 */

class TaskNew extends Component {
  task = {
    name: '',
    description: '',
    priority: 0,
    startDate: '',
    endDate: '',
    categoryId: null,
  };

  defaultCategory = [];

  categorys = [];

  render() {
    if (!this.task) return null;

    return (
      <div>
        {' '}
        <Card title="New task">
          <Row>
            <Form.Textarea
              type="text"
              id={'NewTask'}
              defaultValue={this.task.name}
              onChange={(event) => (this.task.name = event.currentTarget.value)}        // Addes value for task name
            >
              <label>Task-name:</label>
            </Form.Textarea>
            <Row>
              <Column>
                <Form.Label>Start date</Form.Label>
                <Form.Input
                  type="date"
                  pattern={"[0-9]{4}-[0-9]{2}-[0-9]{2}"}
                  id={'StartDate'}
                  value={this.task.startDate}
                  onChange={(event) => (this.task.startDate = event.currentTarget.value)}     // Addes value for task start date
                />
              </Column>
              <Column>
                <Form.Label>End date</Form.Label>
                <Form.Input
                  type="date"
                  pattern={"[0-9]{4}-[0-9]{2}-[0-9]{2}"}
                  id={'EndDate'}
                  value={this.task.endDate}
                  onChange={(event) => (this.task.endDate = event.currentTarget.value)}          // Addes value for task end date
                />
              </Column>
            </Row>

            <Row>
              <Column>
                <Form.Label>Category</Form.Label>
                <select
                  className="form-select"
                  aria-label="Default select example"
                  onChange={(event) => (this.task.categoryId = event.currentTarget.value)}       // Changes the value of category.id when the select value is changed
                >
                  {this.categorys.map((category) => (                                             // Lists all the categories into the select tag
                    <option key={category.id} value={category.id}>
                      {category.name}
                    </option>
                  ))}
                </select>
              </Column>

              <Column>
                <Form.Label>Priority</Form.Label>
                <Form.Switch
                  defaultChecked={this.task.priority}
                  onChange={(event) => {                                                                // Changes the value of the existing priority
                    this.task.priority == 1 ? (this.task.priority = 0) : (this.task.priority = 1);
                  }}
                ></Form.Switch>
              </Column>
            </Row>
            <Row>
              <Form.Label></Form.Label>
              <Form.Textarea
                value={this.task.description}
                onChange={(event) => (this.task.description = event.currentTarget.value)}             // Addes a value for Discription
              >
                <label>Description</label>
              </Form.Textarea>
            </Row>
          </Row>
        </Card>
        <Card>
          <Row>
            <Column>
              <Button.Success
                onClick={() => {                                                             // Colors the input borders with red if they are not filled (required attribute wouldnt work for some mystifistisk resions)
                  if (this.task.name == '') {
                    Alert.info('You must specify task name');
                    document.getElementById('NewTask').style.borderColor = 'red';
                    if (this.task.startDate == '') {
                      document.getElementById('StartDate').style.borderColor = 'red';
                    } else if (this.task.endDate == '') {
                      document.getElementById('EndDate').style.borderColor = 'red';
                    }
                    if (this.task.startDate == '' && this.task.endDate == '') {
                      document.getElementById('StartDate').style.borderColor = 'red';
                      document.getElementById('EndDate').style.borderColor = 'red';
                    }
                  } else if (this.task.startDate == '' || this.task.endDate == '') {
                    Alert.info('You must specify task date');
                    this.task.startDate == ''
                      ? (document.getElementById('StartDate').style.borderColor = 'red')
                      : '';
                    this.task.endDate == ''
                      ? (document.getElementById('EndDate').style.borderColor = 'red')
                      : '';
                    if (this.task.startDate == '' && this.task.endDate == '') {
                      document.getElementById('StartDate').style.borderColor = 'red';
                      document.getElementById('EndDate').style.borderColor = 'red';
                    }
                  } else {
                    this.save();
                  }
                }}
              >
                Save
              </Button.Success>
            </Column>
            <Column right>
              <Button.Light onClick={this.cancel}>Cancel</Button.Light>
            </Column>
          </Row>
        </Card>
      </div>
    );
  }

  mounted() {
    categoryService
      .getCategorys()
      .then((categorys) => {
        this.categorys = categorys;
        if (this.task.categoryId == null) {
          this.task.categoryId = categorys[0].id;
        }
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  save() {
    if(this.task.endDate < this.task.startDate){
      Alert.danger("End date cannot be before start date")}
      else {
    taskService.newTask(this.task).then(() => {
      history.push('/tasks');
    });}
  }
  cancel() {
    history.push('/tasks');
  }
}

export default TaskNew;   // Exports this Componant to be used in index.js
