import * as React from 'react';
import { Component } from 'react-simplified';
import { taskService, newTaskService } from './services';
import { Alert, Card, Row, Column, Button, Icon } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders TaskDetails component
 *
 * Methods:
 *
 * delete() -> Deletes task and redirects user to TaskList
 *
 * complete() -> runs adddate() to add completed date to the task, generates the same task in the CompletedTasks database. Deletes the task from the Tasks database
 *
 * edit() -> redirects the user to the TaskEdit component
 *
 * adddate() -> Generates date and time at the moment it is executed.
 */

class TaskDetails extends Component {
  task = null;
  category = [];

  render() {
    if (!this.task) return null;
    if (this.task.priority == 1) {
      this.prio = <Icon.Priority />;
    } else {
      this.prio = <Icon.NoPriority />;
    }

    return (
      <div>
        <Card title="Task details">
          <Row>
            <Column width={3}>Name:</Column>
            <Column>{this.task.name}</Column>
          </Row>
          <Row>
            <Column width={3}>Category:</Column>
            <Column>{this.category.name}</Column>
          </Row>
          <Row>
            <Column width={3}>Start-date</Column>
            <Column>{this.task.startDate}</Column>
          </Row>
          <Row>
            <Column width={3}>End-date</Column>
            <Column>{this.task.endDate}</Column>
          </Row>
          <Row>
            <Column width={3}>Priority</Column>
            <Column>{this.prio}</Column>
          </Row>
          <Row>
            <Column width={3}>Description:</Column>
            <Column>{this.task.description}</Column>
          </Row>
          <br />
          <Button.Success onClick={this.complete}>Complete Task</Button.Success>
        </Card>
        <Card>
          <Row>
            <Column left>
              <Button.Light onClick={this.edit}>Edit</Button.Light>
            </Column>
            <Column right>
              <Button.Danger onClick={this.delete}>Delete</Button.Danger>
            </Column>
          </Row>
        </Card>
      </div>
    );
  }

  mounted() {
    taskService
      .getTask(this.props.match.params.id)
      .then((task) => {
        this.task = task;

        taskService
          .getCategory(task.categoryId)
          .then((category) => {
            this.category = category;
          })
          .catch((error) => {
            Alert.danger(error.message);
          });
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  edit() {
    history.push('/tasks/' + this.task.id + '/edit');
  }
  delete() {
    taskService
      .deleteTask(this.task)
      .then(history.push('/tasks/'))
      .catch((error) => {
        Alert.danger(error.message);
      });
  }
  complete() {
    this.adddate();

    newTaskService
      .completeTask(this.task)
      .then()
      .catch((error) => {
        Alert.danger(error.message);
      });
    taskService
      .deleteTask(this.task)
      .then(() => {
        history.push('/tasks');
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }
  adddate() {
    var date = new Date();

    var months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    var Completedates = String(
      `${date.getFullYear()}-${months[date.getMonth()]}-${('0' + date.getDate()).slice(-2)} ${(           // Addes a 0 and showes only the first 2 digits
        '0' + date.getHours()
      ).slice(-2)}:${('0' + date.getMinutes()).slice(-2)}`
    );
    if (
      this.task.endDate ==
      `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${date.getDate()}`
    ) {
      console.log('yes');
    }
    this.task.Completedate = Completedates;
  }
}

export default TaskDetails;     // Exports this Componant to be used in index.js
