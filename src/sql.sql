CREATE TABLE Tasks (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name text NOT NULL,
  description text NOT NULL,
  priority Boolean NOT NULL,
  startDate text NOT NULL,
  endDate text NOT NULL,
  Completedate text,
  categoryId INT NOT NULL
);

INSERT INTO Tasks (name, description, priority, startDate, endDate, categoryId) VALUES ('Rydd kjøkken', 'Husk å rydd på kjøkkenet', 1, '24.03.2021', '27.03.2021', 1);
INSERT INTO Tasks (name, description, priority, startDate, endDate, categoryId) VALUES ('Tøm søppel', 'Husk å tøm søppel', 0, '14.03.2024', '24.01.2021', 2);
INSERT INTO Tasks (name, description, priority, startDate, endDate, categoryId) VALUES ('Bestill flybillett', 'Husk å Bestill flybillett', 0, '04.03.2022', '24.011.2021', 2);

CREATE TABLE Categorys (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name text NOT NULL,
  description text NOT NULL
);


INSERT INTO Categorys (name, description) VALUES ('Skole', 'Denne Kategorien inneholder skole tasks');
INSERT INTO Categorys (name, description) VALUES ('Jobb', 'Denne Kategorien inneholder jobb tasks');
INSERT INTO Categorys (name, description) VALUES ('NTNU', 'Denne Kategorien inneholder NTNU tasks');


CREATE TABLE CompletedTasks (
  id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name text NOT NULL,
  description text NOT NULL,
  priority Boolean NOT NULL,
  startDate text NOT NULL,
  endDate text NOT NULL,
  Completedate text NOT NULL,
  categoryId INT NOT NULL
);

