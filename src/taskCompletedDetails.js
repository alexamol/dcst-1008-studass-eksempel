import * as React from 'react';
import { Component } from 'react-simplified';
import { completedTaskService } from './services';
import { Alert, Card, Row, Column, Button, Icon } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders TaskCompletedDetails component
 *
 * Methods:
 *
 * delete() -> Deletes task and redirects the user to TaskCompleted
 */

class TaskCompletedDetails extends Component {
  task = null;
  category = [];

  render() {
    if (!this.task) return null;
    if (this.task.priority == 1) {           // Converts the true value of priority to a filled star
      this.prio = <Icon.Priority />;
    } else {
      this.prio = <Icon.NoPriority />;                // Converts the false value of priority to an empty star
    }
    return (
      <div>
        <Card title="Completed task details">
          <Row>
            <Column width={3}>Name:</Column>
            <Column>{this.task.name}</Column>
          </Row>
          <Row>
            <Column width={3}>Category:</Column>
            <Column>{this.category.name}</Column>
          </Row>
          <Row>
            <Column width={3}>Start-date:</Column>
            <Column>{this.task.startDate}</Column>
          </Row>
          <Row>
            <Column width={3}>End-date:</Column>
            <Column>{this.task.endDate}</Column>
          </Row>
          <Row>
            <Column width={3}>Priority:</Column>
            <Column>{this.prio}</Column>
          </Row>
          <Row>
            <Column width={3}>Completed:</Column>
            <Column>{this.task.Completedate}</Column>
          </Row>
          <Row>
            <Column width={3}>Description:</Column>
            <Column>{this.task.description}</Column>
          </Row>
        </Card>
        <Card>
          <Row>
            <Column right>
              <Button.Danger onClick={this.delete}>Delete from completed tasks</Button.Danger>
            </Column>
          </Row>
        </Card>
      </div>
    );
  }

  mounted() {
    completedTaskService
      .getTask(this.props.match.params.id)
      .then((task) => {
        this.task = task;

        completedTaskService
          .getCategory(task.categoryId)
          .then((category) => {
            this.category = category;
          })
          .catch((error) => {
            Alert.danger(error.message);
          });
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  delete() {
    completedTaskService
      .deleteTask(this.task)
      .then(history.push('/completed/'))
      .catch((error) => {
        Alert.danger(error.message);
      });
  }
}

export default TaskCompletedDetails;    // Exports this Componant to be used in index.js
