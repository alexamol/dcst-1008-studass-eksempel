import * as React from 'react';
import { Component } from 'react-simplified';
import { categoryService } from './services';
import { Alert, Card, Row, Column, Button, Form } from './widgets';
import { createHashHistory } from 'history';

const history = createHashHistory();

/**
 * Renders CategoryEdit Component
 *
 * Methods:
 *
 * save() -> Saves changes to the database and directs user to CategoryList
 *
 * cancel() -> Directs user to CategoryList
 */

class CategoryEdit extends Component {
  category = null;

  render() {
    if (!this.category) return null;

    return (
      <div>
        <Card title="Edit Category">
          <Form.Textarea
            type="text"
            id={"CategoryName"}
            value={this.category.name}                                                      
            onChange={(event) => (this.category.name = event.currentTarget.value)}          // Changes the value of the name for a specific cateogory
          >
            <label>Category-name</label>
          </Form.Textarea>

          <Row>
            <Form.Textarea
              defaultValue={this.category.description}
              onChange={(event) => (this.category.description = event.currentTarget.value)}        // Changes the value of the description for a specific cateogory
            >
              <label>Description</label>
            </Form.Textarea>
          </Row>
        </Card>
        <Row>
          <Column>
            <Button.Success
              onClick={() => {
                if (this.category.name == '') {
                  Alert.info('You must specify category name');
                  document.getElementById('CategoryName').style.borderColor = 'red'
                } else {
                  this.save();
                }
              }}
            >
              Save
            </Button.Success>
          </Column>
          <Column right>
            <Button.Light onClick={this.cancel}>Cancel</Button.Light>
          </Column>
        </Row>
      </div>
    );
  }

  mounted() {
    categoryService
      .getCategory(this.props.match.params.id)
      .then((category) => {
        this.category = category;
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  save() {
    categoryService
      .editCategory(this.category)
      .then(() => {
        history.push('/categorys/');
      })
      .catch((error) => {
        Alert.danger(error.message);
      });
  }

  cancel() {
    history.push('/categorys/');
  }
}

export default CategoryEdit;         // Exports this Componant to be used in index.js
